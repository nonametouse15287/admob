﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Models
{
    public class ResultDetail
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Version { get; set; }
        public int Total { get; set; }
        public object Details { get; set; }

    }
}
