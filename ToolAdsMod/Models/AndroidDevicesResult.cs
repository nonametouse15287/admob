﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Models
{
    public class AndroidDevicesResult
    {
        public bool click { get; set; }
        public AndroidDevice DeviceInfo { get; set; }
    }
}
