﻿
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using ToolAdsMod.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using ToolAdsMod.Models;

namespace ToolAdsMod
{
    public partial class frmMain : Form
    {
        SettingModels _objSetingModels = new SettingModels();
        List<string> lstNameEmail = new List<string>();
        List<NoxModels> lstNoxManage = new List<NoxModels>();
        ThreadManager lstThreadManage = new ThreadManager();
        System.Timers.Timer _aTimerThread = new System.Timers.Timer();
        string currentImages = Path.Combine(Application.StartupPath, @"step_image\checkreg");
        string currentCommand = "";
        int indexEmail = 0;
        System.Timers.Timer _aTimerAutoChangeModem = new System.Timers.Timer();
        string CheckAddAGoogleAcount = "";
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //DComHelper.StopAdb();

            DComHelper.RunCMDV2($"taskkill /F /IM adb.exe");

            string strIndex = FileSystemHelper.GetFileIndex("indexFileName");
            if (!string.IsNullOrEmpty(strIndex))
            {
                int.TryParse(strIndex, out indexEmail);
            }

            currentCommand = '"' + Application.StartupPath + @"\adb.exe" + '"';
            CheckStepHelper.currentCommand = currentCommand;

            _aTimerThread.Interval = 10000;
            _aTimerThread.Elapsed += ProcessManageThreadNox;
            _aTimerThread.Start();

            _objSetingModels = FileSystemHelper.GetSetting();
            ShowSetting();

            #region Start ADB
            Thread ts = new Thread(() =>
                {
                    DComHelper.RunCMDReturnResult($"{currentCommand} start-server");
                });
            ts.IsBackground = true;
            ts.Start();
            #endregion
        }


        private void SetDescriptionIp(string value)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                //lblDescriptionIp.Text = string.Format("Balance: {0}$", value);
            });
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want STOP and Kill All???", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                _aTimerThread.Enabled = false;
                for (int i = lstThreadManage.GenerListThreads.Count - 1; i >= 0; i--)
                {
                    lstThreadManage.GenerListThreads[i].Status = 1;
                }

                foreach (ListViewItem item in lstViewDevices.Items)
                {
                    item.SubItems[2].Text = "STOPED";
                }
            }
        }

        private bool CheckConnect(string host)
        {
            bool checkCon = false;
            string strCheck = DComHelper.RunCMDReturnResult($"{currentCommand} devices");
            if (strCheck.Replace("\n", "").Replace("\r", "").Contains(host))
            {
                checkCon = true;
            }
            return checkCon;
        }

        private void ConnectDevices(string host)
        {
            string strCheck = DComHelper.RunCMDReturnResult($"{currentCommand} connect {host}");
        }


        private void ProcessManageThreadNox(object sender, ElapsedEventArgs e)
        {
            _aTimerThread.Stop();
            try
            {

                //DeviceHelper.GetListDevicesAdb();
                /*Check Devices*/
                List<DevicesModels> lstDevices = GetConnectionDevices();

                List<string> lstDevicesFromFile = FileSystemHelper.GetDevicesFromFile();

                foreach (var item in lstDevicesFromFile)
                {
                    bool checkExits = false;
                    foreach (var objThreadManage in lstThreadManage.GenerListThreads)
                    {
                        if (objThreadManage.DevicesSerial == item)
                        {
                            checkExits = true;
                            break;
                        }
                    }

                    if (!checkExits)
                    {
                        ConnectDevices(item);
                    }
                }

                foreach (var item in lstDevices)
                {
                    string deviceName = FileSystemHelper.GetListDevices(item.DeviceCode);
                    string deviceProxy = FileSystemHelper.GetProxyDevices(item.DeviceCode);
                    string[] row = { item.DeviceCode, item.DeviceStatus == "device" ? "Online" : "Offline", "NOT START", "M_" + deviceName, deviceProxy };
                    ListViewItem lstItem = new ListViewItem(row);
                    var devcies = lstThreadManage.GenerListThreads.Where(u => u.DevicesSerial == item.DeviceCode).FirstOrDefault();
                    if (devcies == null)
                    {
                        lstItem.SubItems[2].Text = "READY...";
                        ThreadInfo objThreadManage = new ThreadInfo();
                        objThreadManage.DevicesItems = lstItem;
                        objThreadManage.DevicesSerial = lstItem.SubItems[0].Text;
                        objThreadManage.DevicesProxy = deviceProxy;
                        ThreadStart t = new ThreadStart(() =>
                        {
                            ProcessToolAdsMod(lstItem);
                        });

                        objThreadManage.GenThreadInfo = new Thread(t);
                        lstThreadManage.GenerListThreads.Add(objThreadManage);
                        objThreadManage.GenThreadInfo.IsBackground = true;
                        objThreadManage.Status = 1;
                        objThreadManage.GenThreadInfo.Start();

                        AddListView(lstItem);
                    }
                }
            }
            catch (Exception)
            {

            }
            _aTimerThread.Start();
        }

        private void AddListView(ListViewItem items)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                bool iCheck = false;
                foreach (ListViewItem item in lstViewDevices.Items)
                {
                    if (item.SubItems[0].Text == items.SubItems[0].Text)
                    {
                        iCheck = true;
                    }
                }
                if (!iCheck)
                {
                    lstViewDevices.Items.Add(items);
                }
            });
        }

        private void ProcessToolAdsMod(object sender)
        {
            string countryCode = "vn";
            string countryName = "Viet nam";
            ListViewItem objModels = (ListViewItem)sender;
            string host = objModels.SubItems[0].Text;
            string[] arrayHost = host.Split(':');
            string hostfolder = arrayHost[0];
            if (arrayHost.Length > 1)
            {
                hostfolder = hostfolder + arrayHost[1];
            }

            int iCountStep = 0;

        CheckSTATUS:

            string fullCountry = FileSystemHelper.GetCountryRandom();
            countryCode = fullCountry.Split('|')[1];
            countryName = fullCountry.Split('|')[0];

            var threadAction = lstThreadManage.GenerListThreads.FirstOrDefault(u => u.DevicesSerial == host);

            if (!CheckConnect(host))
            {
                threadAction.Status = 3;
                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[1].Text = "Offline";
                    objModels.SubItems[2].Text = "DISCONNECT....";
                });
            }
            else if (threadAction.Status == 3)
            {
                threadAction.Status = 0;

                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[1].Text = "Online";
                    //objModels.SubItems[2].Text = "DISCONNECT....";
                });
            }


            if (threadAction.Status != 0)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "STOPED...";
                });
                Thread.Sleep(2000);
                goto CheckSTATUS;
            }


            string folderImages = Path.Combine(Application.StartupPath, "pic_gmail");
            folderImages = Path.Combine(folderImages, host.Replace(":", ""));
            FileSystemHelper.CreateFolder(folderImages);
            //CheckStepHelper.CheckOrange(folderImages, host);
            /*Call API get devices*/

            this.Invoke((MethodInvoker)delegate ()
            {
                objModels.SubItems[1].Text = "Online";
            });

            try
            {
                //CheckStepHelper.CheckOrange(folderImages, host);
                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "BAT DAU QUA TRINH...";
                });
                Thread.Sleep(1000);
                SwitchTab(host);
                Thread.Sleep(1000);                           

                #region DELETE PACKAGE
                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "START DELETE PACKAGE...";
                });

                CleanCache(host);

                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "DELETE SUCCESS PACKAGE...";
                });

                Thread.Sleep(2000);

                #endregion

                #region DELETE Google Advertising

                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "DELETE GOOGLE";
                });

                killGoogleAdvertisingId(host);

                Thread.Sleep(1000);

                #endregion
                
                #region OPEN APP HMA

                if (threadAction.DevicesProxy == "HMA")
                {

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "OPEN APP HMA...";
                    });

                    mnOpenHMAAction(host, objModels, countryName.Replace(" ", "%s"));

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "END APP HMA...";
                    });

                    Thread.Sleep(1000);
                }
                else
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "OPEN APP PVA...";
                    });

                    mnOpenPVAAction(host, objModels);

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "END APP PVA...";
                    });

                    Thread.Sleep(1000);

                }

                #endregion

                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "GET DEVICES INFO...";
                });

                AndroidDevicesResult objDevices = ApiHelper.DevicesGet(countryCode);

                if (objDevices != null)
                {
                    //createDeviceFolder(host);                   

                    #region OPEN APP CHANGE DEVICES
                    this.Invoke((MethodInvoker)delegate ()
                               {
                                   objModels.SubItems[2].Text = "DELETE FILE DEVICES...";
                               });

                    DeleteFileDevices(host);

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "PUSH FILE TO MOBILE...";
                    });

                    Thread.Sleep(2000);
                    string jsonDevices = JsonConvert.SerializeObject(objDevices.DeviceInfo);

                    PushFileDevices(folderImages, host, jsonDevices);
                    Thread.Sleep(2000);

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "OPEN APP CHANGE DEVICES...";
                    });

                    Thread.Sleep(2000);

                    packageToOpen(host);

                    Thread.Sleep(10000);

                    #endregion

                    #region OPEN APP AdsMod

                    this.Invoke((MethodInvoker)delegate ()
                    {
                        objModels.SubItems[2].Text = "OPEN APP AdsMod VN...";
                    });

                    //bool clickBanner = false;
                    //bool clickAds = false;

                    //if (objDevices.click == true)
                    //{
                    //    clickAds = true;
                    //    Random r = new Random();
                    //    int iCountRandom = r.Next(0, 1000);
                    //    clickBanner = iCountRandom < 300 ? true : false;
                    //}

                    /*Auto view Ads*/
                    for (int i = 0; i < 4; i++)
                    {
                        mnPENADMOBVNAction(host, objModels);
                        /**/

                        ClickViewContent(host);

                        //if (clickAds == true && i % 2 == 0)
                        //{
                        //    //ClickAddBanner(host);

                        //    mnPENADMOBVNAction(host, objModels);
                        //    if (clickBanner)
                        //    {
                        //        /*Click banner*/
                        //        ClickAdsBanner(host);
                        //    }
                        //    else
                        //    {
                        //        /*Click Ads*/
                        //        ClickAdsContent(host);

                        //        ClickAdsContent(host);

                        //    }
                        //}
                    }

                    #endregion

                }
                else
                {
                    goto CheckSTATUS;
                }

                //Todo: thao tac AdsMod

                this.Invoke((MethodInvoker)delegate ()
                {
                    objModels.SubItems[2].Text = "KET THUC QUA TRINH...";
                });
                Thread.Sleep(5000);
                goto CheckSTATUS;
            }
            catch (Exception ex)
            {

            }

            goto CheckSTATUS;
        }

        private void ClickViewContent(string host)
        {
            Random r = new Random();

            int x = r.Next(50, 650);
            int y = r.Next(400, 1100);
            int iRandomView = 5;

            iRandomView = r.Next(2, 5);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");

            iRandomView = r.Next(0, 10);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} {y}");

            Thread.Sleep(2000);
            iRandomView = r.Next(7, 20);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");

            iRandomView = r.Next(7, 20);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");

            iRandomView = r.Next(7, 15);
            RandDomSwip(host, iRandomView);
        }

        private void ClickAdsBanner(string host)
        {
            Random r = new Random();
            int x = r.Next(250, 650);
            int y = r.Next(400, 1100);
            int iRandomView = 5;


            iRandomView = r.Next(0, 10);
            RandDomSwip(host, iRandomView);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");
            Thread.Sleep(5000);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 600 300 500 1000 70");
            Thread.Sleep(2000);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} 140");
            Thread.Sleep(5000);
            /*Todo: lam them cai gi do*/
            x = r.Next(380, 700);
            y = r.Next(1640, 1700);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} {y}");
            Thread.Sleep(5000);

            x = r.Next(120, 240);
            y = r.Next(1665, 1682);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} {y}");
            Thread.Sleep(2000);

            iRandomView = r.Next(8, 25);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");

            iRandomView = r.Next(7, 15);
            RandDomSwip(host, iRandomView);
        }

        private void ClickAdsContent(string host)
        {
            Random r = new Random();

            int x = r.Next(50, 650);
            int y = r.Next(400, 1100);
            int iRandomView = 5;

            iRandomView = r.Next(2, 5);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} {y}");

            Thread.Sleep(2000);
            iRandomView = r.Next(7, 20);
            RandDomSwip(host, iRandomView);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input keyevent KEYCODE_BACK");

            iRandomView = r.Next(7, 20);
            RandDomSwip(host, iRandomView);

            /*Click quang cao*/
            x = r.Next(50, 650);
            y = r.Next(400, 1100);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap {x} {y}");
            Thread.Sleep(2000);
            iRandomView = r.Next(7, 20);
            RandDomSwip(host, iRandomView);
            /*Todo: lam them cai gi do*/
        }

        private void RandDomSwip(string host, int iRandomView)
        {
            Random r = new Random();
            int timeVuot = r.Next(130, 200);
            for (int i = 0; i < iRandomView; i++)
            {
                timeVuot = r.Next(130, 200);
                int cachVuot = r.Next(0, 100);
                if (cachVuot < 25)
                {
                    DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 500 1000 600 300 {timeVuot}");
                    timeVuot = r.Next(2, 5);
                    Thread.Sleep(timeVuot*1000);
                }
                else if (25 <= cachVuot && cachVuot < 50)
                {
                    DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 500 300 600 1000 {timeVuot}");
                    Thread.Sleep(2000);
                    timeVuot = r.Next(2, 5);
                    Thread.Sleep(timeVuot * 1000);
                }
                else if (50 <= cachVuot && cachVuot < 75)
                {
                    DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 200 800 1000 800 {timeVuot}");                    
                    timeVuot = r.Next(2, 5);
                    Thread.Sleep(timeVuot * 1000);
                }
                else if (75 <= cachVuot && cachVuot < 100)
                {
                    DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 1000 800 200 800 {timeVuot}");
                    timeVuot = r.Next(2, 5);
                    Thread.Sleep(timeVuot * 1000);
                }
            }

            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 600 300 500 1000 70");
            Thread.Sleep(2000);
        }

        private void BuildSetting()
        {
            //_objSetingModels.NamePath = txtNamePath.Text;
            //_objSetingModels.txtApiKey = txtApiKey.Text;
            //_objSetingModels.txtServerKey = txtServerKey.Text;
        }

        private void ShowSetting()
        {
            //txtNamePath.Text = _objSetingModels.NamePath;
            //txtApiKey.Text = _objSetingModels.txtApiKey;
            //txtServerKey.Text = _objSetingModels.txtServerKey;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            BuildSetting();
            string output = JsonConvert.SerializeObject(_objSetingModels);
            FileSystemHelper.SaveSetting(output);
        }

        #region Control Android

        private void RegAccount(string host, string firstname, string lastname, ListViewItem itemView)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                itemView.SubItems[2].Text = "START CREATE ACCOUNT...";
            });

            var threadAction = lstThreadManage.GenerListThreads.FirstOrDefault(u => u.DevicesSerial == host);


            Thread.Sleep(1000);

            CheckConnectThread(host, threadAction, itemView);
        }

        private void CheckConnectThread(string host, ThreadInfo threadAction, ListViewItem itemView)
        {
            if (!CheckConnect(host))
            {
                threadAction.Status = 3;
                this.Invoke((MethodInvoker)delegate ()
                {
                    itemView.SubItems[1].Text = "Offline";
                    itemView.SubItems[2].Text = "DISCONNECT....";
                });
            }
            else if (threadAction.Status == 3)
            {
                threadAction.Status = 0;
            }
        }
        private void ChangeXgame(string host, ListViewItem itemView)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                itemView.SubItems[2].Text = "CHANGE DEVICES...";
            });

            string folderImages = Path.Combine(Application.StartupPath, "pic_gmail");
            folderImages = Path.Combine(folderImages, host.Replace(":", ""));
            //CloseXgame(host);
            OpenXgame(host);

            Thread.Sleep(2000);
            /*Change Info*/
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 250 300");
            //Thread.Sleep(1000);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 800 150");
            Thread.Sleep(14000);
            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 800 150");
            Thread.Sleep(5000);
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 550 440");
            //Thread.Sleep(1000);
            /*Change Lan 2*/
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 250 300");
            //Thread.Sleep(1000);
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 800 150");
            //Thread.Sleep(5000);
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 550 440");
            //Thread.Sleep(5000);

            ///*Wipe data*/
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 801 300");
            //Thread.Sleep(1000);
            //DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 530 145");
            //Thread.Sleep(5000);
            this.Invoke((MethodInvoker)delegate ()
            {
                itemView.SubItems[2].Text = "REBOOT DEVICES...";
            });

            /*Wipe reboot*/
            DComHelper.RunCMD($"{currentCommand} -s {host} reboot");
            Thread.Sleep(5000);

            bool CheckKetNoi = false;

            this.Invoke((MethodInvoker)delegate ()
            {
                itemView.SubItems[2].Text = "CONNECTING DEVICES...";
            });

            while (!CheckKetNoi)
            {
                Thread.Sleep(2000);
                List<DevicesModels> lstDevices = GetConnectionDevices();
                foreach (var item in lstDevices)
                {
                    if (item.DeviceCode == host && item.DeviceStatus == "device")
                    {
                        CheckKetNoi = true;
                        break;
                    }
                }
            }
            Thread.Sleep(1000);
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input swipe 500 1000 600 300 100");
            this.Invoke((MethodInvoker)delegate ()
            {
                itemView.SubItems[2].Text = "CONNECTED DEVICES...";
            });

        }

        public void CloseSettings(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am force-stop com.android.settings");
            Thread.Sleep(1000);
        }

        public void OpenSettings(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am start -a android.settings.SETTINGS");
            //DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am start -n com.android.vending/android.intent.category.LAUNCHER");
            Thread.Sleep(1000);
        }

        public List<DevicesModels> GetConnectionDevices()
        {
            List<DevicesModels> lstDevices = new List<DevicesModels>();
            string result = DComHelper.RunCMDV2($"{currentCommand} devices");
            string lststring = result.Replace("\r\n", "|");
            string[] arrayDevices = result.Replace("\r\n", "|").Split('|');
            foreach (var item in arrayDevices)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    string[] arraySplit = item.Replace("\t", "|").Split('|');
                    if (arraySplit.Length == 2)
                    {
                        DevicesModels objDevices = new DevicesModels();
                        objDevices.DeviceCode = arraySplit[0];
                        objDevices.DeviceStatus = arraySplit[1];
                        lstDevices.Add(objDevices);
                    }
                }
            }

            return lstDevices;
        }

        public void CloseXgame(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am force-stop app.haonam.xgame");
            Thread.Sleep(1000);
        }

        public void ClosePackage(string host, string package)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am force-stop " + package);
            Thread.Sleep(1000);
        }



        public void OpenXgame(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am start -n app.haonam.xgame/.MainActivity");
            Thread.Sleep(1000);
        }

        public void DeleteFileDevices(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell rm -r /sdcard/device.json");
            Thread.Sleep(1000);
        }

        public void SwitchTab(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell input keyevent KEYCODE_APP_SWITCH");
            Thread.Sleep(2000);

            DComHelper.RunCMD($"{currentCommand} -s {host} shell input tap 947 1432");
            Thread.Sleep(2000);
        }

        private void CleanCache(string host)
        {
            List<string> listPackage = listCleanPackage();
            for (int i = 0; i < listPackage.Count; i++)
            {
                ClosePackage(host, listPackage[i].ToString());
                DComHelper.RunCMDV2($"{currentCommand} -s {host} shell pm clear " + listPackage[i].ToString());
                Thread.Sleep(1000);
            }
        }

        private List<string> listCleanPackage()
        {
            List<string> listPackage = new List<string>();
            listPackage.Add("com.android.vending");
            listPackage.Add("com.google.android.gsf");
            listPackage.Add("com.google.android.gsf.login");
            listPackage.Add("com.google.android.gms");
            listPackage.Add("com.google.android.gm");
            listPackage.Add("com.qualcomm.location");
            listPackage.Add("com.google.android.play.games");
            listPackage.Add("com.android.chrome");
            listPackage.Add("com.android.settings");
            listPackage.Add("android.webkit");
            listPackage.Add("com.google.android.webview");
            return listPackage;
        }


        private void packageToOpen(string host)
        {
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell am start -n com.hac.device.changer/.MainActivity");
            Thread.Sleep(1000);
        }

        private string getPID(string package, string host)
        {
            string output = string.Empty;
            string err = string.Empty;
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell ps |grep {host} |awk " + "'{print $2}'", ref output, ref err);
            return output;
        }

        private void killGoogleAdvertisingId(string host)
        {

            DComHelper.RunGoogleClean($"{currentCommand} -s {host} shell");
            Thread.Sleep(1000);
        }

        private void createDeviceFolder(string host)
        {
            DComHelper.RunCreateFolder($"{currentCommand} -s {host} shell");
            Thread.Sleep(1000);
        }

        public void PushFileDevices(string filePath, string host, string jsonDevices)
        {
            //**Delete file Images
            string fileFullPath = filePath + @"\device.json";

            string fileBatFullPath = filePath + @"\device.bat";

            //string fileFullPath = Application.StartupPath + @"\device.json";
            using (FileStream fs = new FileStream(fileFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamWriter sr = new StreamWriter(fileFullPath, false))
            {
                sr.WriteLine(jsonDevices);
            }

            using (FileStream fs = new FileStream(fileBatFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamWriter sr = new StreamWriter(fileBatFullPath, false))
            {
                sr.WriteLine($"{currentCommand} -s {host} push \"{fileFullPath}\" /sdcard/");
            }

            DComHelper.RunCMDV2(fileBatFullPath);
        }
        #endregion

        private void mnStart_Click(object sender, EventArgs e)
        {
            CheckRequied();

            if (lstViewDevices.SelectedItems.Count > 0)
            {
                StartThread(lstViewDevices.SelectedItems[0]);
            }
        }

        private void mnStop_Click(object sender, EventArgs e)
        {
            if (lstViewDevices.SelectedItems.Count > 0)
            {
                string serial = lstViewDevices.SelectedItems[0].SubItems[0].Text;
                KillThread(serial);
                lstViewDevices.SelectedItems[0].SubItems[2].Text = "STOPPING....";
            }
        }

        private void StartThread(ListViewItem keyDevices)
        {
            var objNoxAdd = lstThreadManage.GenerListThreads.FirstOrDefault(u => u.DevicesSerial == keyDevices.Text);
            if (objNoxAdd != null)
            {
                objNoxAdd.Status = 0;
            }
        }

        private void KillThread(string keyDevices)
        {
            for (int i = lstThreadManage.GenerListThreads.Count - 1; i >= 0; i--)
            {
                if (lstThreadManage.GenerListThreads[i].DevicesSerial == keyDevices)
                {
                    lstThreadManage.GenerListThreads[i].Status = 1;
                    //lstThreadManage.GenerListThreads[i].GenThreadInfo.Abort();
                }
            }
        }

        private void CheckRequied()
        {

            //if (lstNameEmail.Count == 0)
            //{
            //    if (txtNamePath.Text != "")
            //    {
            //        string[] readText = File.ReadAllLines(txtNamePath.Text);
            //        foreach (string s in readText)
            //        {
            //            lstNameEmail.Add(s);
            //        }
            //    }
            //}

        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Do you want close app???", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                _aTimerThread.Enabled = false;
                for (int i = lstThreadManage.GenerListThreads.Count - 1; i >= 0; i--)
                {
                    //lstThreadManage.GenerListThreads[i].Status = 1;
                    lstThreadManage.GenerListThreads[i].GenThreadInfo.Abort();
                    lstThreadManage.GenerListThreads.Remove(lstThreadManage.GenerListThreads[i]);
                }

                DComHelper.StopAdb();
                //btnStart.Enabled = true;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void mnRemove_Click(object sender, EventArgs e)
        {
            if (lstViewDevices.SelectedItems.Count > 0)
            {
                string serial = lstViewDevices.SelectedItems[0].SubItems[0].Text;
                lstViewDevices.SelectedItems[0].SubItems[2].Text = "REMOVING....";
                for (int i = lstThreadManage.GenerListThreads.Count - 1; i >= 0; i--)
                {
                    if (lstThreadManage.GenerListThreads[i].DevicesSerial == serial)
                    {
                        lstThreadManage.GenerListThreads[i].Status = 1;
                        lstThreadManage.GenerListThreads[i].GenThreadInfo.Abort();
                        lstThreadManage.GenerListThreads.Remove(lstThreadManage.GenerListThreads[i]);
                    }
                }
                lstViewDevices.Items.Remove(lstViewDevices.SelectedItems[0]);
            }
        }


        private void mnOpenPVAAction(string serial, ListViewItem listView)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "START OPEN VPN....";
            });
            DComHelper.RunCMDV2($"{currentCommand} -s {serial} shell monkey -p com.privateinternetaccess.android -c android.intent.category.LAUNCHER 1");
            Thread.Sleep(4000);
            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "VPN OPENED....";
            });
            Thread.Sleep(1000);
            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "START CHANGE COUNTRY....";
            });
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 500 1550");
            Thread.Sleep(2000);

            int randomCountry = new Random().Next(2, 5);
            for (int i = 0; i < randomCountry; i++)
            {
                int ranScroll = new Random().Next(100, 250);
                DComHelper.RunCMDV2($"{currentCommand} -s {serial} shell input swipe 500 1000 600 300 " + ranScroll);
                Thread.Sleep(2000);
            }

            int ranTouch = new Random().Next(300, 1800);
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 500 " + ranTouch);
            Thread.Sleep(10000);
        }

        private void mnPENADMOBVNAction(string serial, ListViewItem listView)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "START ADMOB VN....";
            });

            DComHelper.RunCMDV2($"{currentCommand} -s {serial} shell monkey -p com.hac.breakingnews.gui -c android.intent.category.LAUNCHER 1");
            Thread.Sleep(10000);
        }

        private void mnOpenHMAAction(string serial, ListViewItem listView, string countryName)
        {


            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "START OPEN HMA....";
            });
            DComHelper.RunCMDV2($"{currentCommand} -s {serial} shell monkey -p com.hidemyass.hidemyassprovpn -c android.intent.category.LAUNCHER 1");
            Thread.Sleep(15000);

            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "VPN HMA....";
            });
            Thread.Sleep(1000);

            this.Invoke((MethodInvoker)delegate ()
            {
                listView.SubItems[2].Text = "START CHANGE COUNTRY....";
            });
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 1021 153");
            Thread.Sleep(2000);
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 551 1663");
            Thread.Sleep(5000);
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 520 332");
            Thread.Sleep(5000);
            /*Send search country*/
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap 1000 150");
            Thread.Sleep(2000);
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input text '{countryName}'");
            Thread.Sleep(2000);
            //int randomCountry = new Random().Next(2, 15);

            //for (int i = 0; i < randomCountry; i++)
            //{
            //    int ranScroll = new Random().Next(100, 250);
            //    DComHelper.RunCMDV2($"{currentCommand} -s {serial} shell input swipe 500 1000 600 300 " + ranScroll);
            //    Thread.Sleep(2000);
            //}

            int ranTouch = new Random().Next(300, 700);
            DComHelper.RunCMD($"{currentCommand} -s {serial} shell input tap {ranTouch} 650");
            Thread.Sleep(10000);

        }

    }
}
