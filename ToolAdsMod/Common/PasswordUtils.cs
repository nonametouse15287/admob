﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public static class PasswordUtils
    {
        private const string charset = "0123456789abcdefghijklmnopqrstuvwxyz";
        private const string charset2 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static string GenerateRequestKey()
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < 10; i++)
            {
                var index = random.Next(0, charset.Length);
                builder.Append(charset[index]);
            }

            return builder.ToString();
        }

        public static string GenerateRequestKey(int icharacter)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < icharacter; i++)
            {
                var index = random.Next(0, charset.Length);
                builder.Append(charset[index]);
            }

            return builder.ToString();
        }

        public static string GenerateRequestKey2(int icharacter)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < icharacter; i++)
            {
                var index = random.Next(0, charset2.Length);
                builder.Append(charset2[index]);
            }

            return builder.ToString();
        }

        public static string GeneratePassword()
        {
            string charsetP1 = "abcdefghijklmnopqrstuvwxyz";
            string charsetP2 = "0123456789";
            string charsetP3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string charsetP4 = "0123456789";

            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < 3; i++)
            {
                var index = random.Next(0, charsetP1.Length);
                builder.Append(charsetP1[index]);
            }

            for (var i = 0; i < 3; i++)
            {
                var index = random.Next(0, charsetP2.Length);
                builder.Append(charsetP2[index]);
            }

            for (var i = 0; i < 3; i++)
            {
                var index = random.Next(0, charsetP3.Length);
                builder.Append(charsetP3[index]);
            }

            for (var i = 0; i < 3; i++)
            {
                var index = random.Next(0, charsetP4.Length);
                builder.Append(charsetP4[index]);
            }

            return builder.ToString();
        }

        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

    }
}
