﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public class TwoCaptchaClient
    {
        public string APIKey { get; set; } = "xxxx";
        public string GoogleKey { get; set; } = "xxxx";
        public TwoCaptchaClient(string apiKey)
        {
            APIKey = apiKey;
        }

        /// <summary>
        /// Sends a solve request and waits for a response
        /// </summary>
        /// <param name="googleKey">The "sitekey" value from site your captcha is located on</param>
        /// <param name="pageUrl">The page the captcha is located on</param>
        /// <param name="proxy">The proxy used, format: "username:password@ip:port</param>
        /// <param name="proxyType">The type of proxy used</param>
        /// <param name="result">If solving was successful this contains the answer</param>
        /// <returns>Returns true if solving was successful, otherwise false</returns>
        public bool SolveRecaptchaV2(string googleKey, string pageUrl, string proxy, ProxyType proxyType, out string result, out string idcapcha)
        {
            //string requestUrl = "http://2captcha.com/in.php?key=" + APIKey + "&method=userrecaptcha&googlekey=" + GoogleKey + "&pageurl=" + pageUrl + "&proxy=" + proxy + "&proxytype=";
            string requestUrl = "http://2captcha.com/in.php?key=" + APIKey + "&method=userrecaptcha&googlekey=" + googleKey + "&pageurl=" + pageUrl + "&here=now";

            switch (proxyType)
            {
                case ProxyType.HTTP:
                    requestUrl += "HTTP";
                    break;
                case ProxyType.HTTPS:
                    requestUrl += "HTTPS";
                    break;
                case ProxyType.SOCKS4:
                    requestUrl += "SOCKS4";
                    break;
                case ProxyType.SOCKS5:
                    requestUrl += "SOCKS5";
                    break;
            }

            try
            {
                WebRequest req = WebRequest.Create(requestUrl);

                using (WebResponse resp = req.GetResponse())
                using (StreamReader read = new StreamReader(resp.GetResponseStream()))
                {
                    string response = read.ReadToEnd();

                    if (response.Length < 3)
                    {
                        result = response;
                        idcapcha = "Loi khong request duoc";
                        return false;
                    }
                    else
                    {
                        if (response.Substring(0, 3) == "OK|")
                        {
                            string captchaID = response.Remove(0, 3);
                            idcapcha = captchaID;
                            for (int i = 0; i < 24; i++)
                            {
                                WebRequest getAnswer = WebRequest.Create("http://2captcha.com/res.php?key=" + APIKey + "&action=get&id=" + captchaID);

                                using (WebResponse answerResp = getAnswer.GetResponse())
                                using (StreamReader answerStream = new StreamReader(answerResp.GetResponseStream()))
                                {
                                    string answerResponse = answerStream.ReadToEnd();

                                    if (answerResponse.Length < 3)
                                    {
                                        result = answerResponse;
                                        return false;
                                    }
                                    else
                                    {
                                        if (answerResponse.Substring(0, 3) == "OK|")
                                        {
                                            result = answerResponse.Remove(0, 3);
                                            return true;
                                        }
                                        else if (answerResponse != "CAPCHA_NOT_READY")
                                        {
                                            result = answerResponse;
                                            return false;
                                        }
                                    }
                                }

                                Thread.Sleep(5000);
                            }

                            result = "Timeout";
                            idcapcha = "3";
                            return false;
                        }
                        else
                        {
                            result = response;
                            idcapcha = "2";
                            return false;
                        }
                    }
                }
            }
            catch { }

            result = "Unknown error";
            idcapcha = "1";
            return false;
        }

        /// <summary>
        /// Slove normal capcha wroted by K9 from Kteam
        /// You have to get the capcha image from the website then convert to base64
        /// </summary>
        /// <param name="base64Image"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool SolveNormalCapcha(string base64Image, out string result, out string captID)
        {
            try
            {
                captID = "";
                string response = "";
                using (var client = new WebClient())
                {
                    var values = new NameValueCollection();
                    values["method"] = "base64";
                    values["key"] = APIKey;
                    values["body"] = base64Image;
                    var res = client.UploadValues("http://2captcha.com/in.php", values);
                    response = Encoding.Default.GetString(res);
                }

                Thread.Sleep(TimeSpan.FromSeconds(5));
                if (response.Substring(0, 3) == "OK|")
                {
                    string captchaID = response.Remove(0, 3);
                    captID = captchaID;

                    for (int i = 0; i < 24; i++)
                    {
                        WebRequest getAnswer = WebRequest.Create("http://2captcha.com/res.php?key=" + APIKey + "&action=get&id=" + captchaID);

                        using (WebResponse answerResp = getAnswer.GetResponse())
                        using (StreamReader answerStream = new StreamReader(answerResp.GetResponseStream()))
                        {
                            string answerResponse = answerStream.ReadToEnd();

                            if (answerResponse.Length < 3)
                            {
                                result = answerResponse;
                                return false;
                            }
                            else
                            {
                                if (answerResponse.Substring(0, 3) == "OK|")
                                {
                                    result = answerResponse.Remove(0, 3);
                                    return true;
                                }
                                else if (answerResponse != "CAPCHA_NOT_READY")
                                {
                                    result = answerResponse;
                                    return false;
                                }
                            }
                        }

                        Thread.Sleep(5000);
                    }

                    result = "Timeout";
                    return false;
                }
                else
                {
                    result = response;
                    return false;
                }
            }
            catch (Exception error)
            {
                FileSystemHelper.SaveFileErrorLog(string.Format("{0:ddMMyyyy hhmmss}|TaoEmail_1_Error", DateTime.Now) +
                        error.Message + "\n InnerException:" + (error.InnerException != null ? error.InnerException.Message : "") +
                        "\n StackTrace:" + error.StackTrace, "LoiHeThong");

                result = "";
                captID = "";
                return false;
            }
        }

        public string ReportBad(string captchaId)
        {
            try
            {
                string result = "";
                if (!string.IsNullOrEmpty(captchaId))
                {
                    using (var webClient = new WebClient())
                    {
                        result = webClient.DownloadString($"http://2captcha.com/res.php?key={APIKey}&action=reportbad&id={captchaId}");
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }

    public enum ProxyType
    {
        HTTP,
        HTTPS,
        SOCKS4,
        SOCKS5
    }
}
