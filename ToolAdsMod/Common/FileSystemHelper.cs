﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace ToolAdsMod.Common
{
    public static class FileSystemHelper
    {
        public static bool KillChildApp(string processName)
        {
            foreach (var tl in Process.GetProcessesByName(processName))
            {
                try
                {
                    tl.Kill();
                    tl.Dispose();
                }
                catch (Exception)
                {
                }
            }
            Thread.Sleep(200);
            return true;
        }

        public static void CreateFolder(string folder)
        {

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        public static void SaveLog(string errror, string folderName, string fileName)
        {
            try
            {
                string strDate = string.Format("{0}_log.txt", fileName);
                string pathFile = $@"{folderName}/{strDate}";
                CreateFolder(folderName);
                /*Create file*/
                using (FileStream fs = new FileStream(pathFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamWriter sr = new StreamWriter(pathFile, true))
                {
                    errror = $"{DateTime.Now:dd MM yyyy HH:mm:ss}|{errror}";
                    sr.WriteLine(errror);
                }
            }
            catch (Exception)
            { }
            finally
            {

            }
        }

        public static void SaveFileErrorLog(string errror, string ProjectName)
        {
            string strDate = string.Format("{0}Error_log.txt", ProjectName);
            string pathFile = $@"{ProjectName}/{strDate}";
            CreateFolder(ProjectName);
            /*Create file*/
            using (FileStream fs = new FileStream(pathFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamWriter sr = new StreamWriter(pathFile, true))
            {
                sr.WriteLine(errror);
            }
        }

        public static void SaveFileLog(string key, string fileName)
        {
            string folderLog = Path.Combine(Application.StartupPath, "log");
            CreateFolder(folderLog);
            fileName = Path.Combine(folderLog, fileName);
            using (FileStream fs = new FileStream(fileName + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamWriter sr = new StreamWriter(fileName + ".txt", true))
            {
                sr.WriteLine(key);
            }
        }

        public static bool CheckFileLog(string key, string fileName)
        {
            bool checkUse = false;
            try
            {
                using (FileStream fs = new FileStream(fileName + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamReader reader = new StreamReader(fileName + ".txt"))
                {
                    while (!reader.EndOfStream)
                    {
                        if (reader.ReadLine().Contains(key))
                        {
                            checkUse = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return checkUse;
        }

        public static SettingModels GetSetting()
        {
            SettingModels objSetingModels = new SettingModels();
            using (FileStream fs = new FileStream("settings.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            string strline = "";
            using (var reader = new StreamReader("settings.txt"))
            {
                while (!reader.EndOfStream)
                {
                    strline = reader.ReadLine();
                }
            }

            if (!string.IsNullOrEmpty(strline))
            {
                try
                {
                    objSetingModels = JsonConvert.DeserializeObject<SettingModels>(strline);
                }
                catch (Exception)
                {

                }
            }
            return objSetingModels;
        }

        public static void SaveSetting(string settings)
        {
            using (FileStream fs = new FileStream("settings.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }

            using (StreamWriter sr = new StreamWriter("settings.txt", false))
            {
                sr.WriteLine(settings);
            }
        }

        public static void SaveFileIndex(string key, string fileName)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamWriter sr = new StreamWriter(fileName + ".txt", false))
                {
                    sr.WriteLine(key);
                }
            }
            catch (Exception)
            {

            }
        }

        public static string GetFileIndex(string fileName)
        {
            string fileIndex = "";
            using (FileStream fs = new FileStream(fileName + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamReader sr = new StreamReader(fileName + ".txt", false))
            {
                fileIndex = sr.ReadToEnd();
            }
            return fileIndex;
        }

        public static string GetListDevices(string deviceCode)
        {
            string fileIndex = "31";
            int iIndex = 0;
            try
            {
                using (FileStream fs = new FileStream("ListDevices.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamReader sr = new StreamReader("ListDevices.txt", false))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] arrayList = sr.ReadLine().Split('|');
                        if (arrayList.Length >= 2)
                        {
                            if (int.TryParse(arrayList[1], out iIndex) && arrayList[0] == deviceCode)
                            {
                                fileIndex = arrayList[1];
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return fileIndex;
        }



        public static string GetProxyDevices(string deviceCode)
        {
            string fileIndex = "PVA";
            int iIndex = 0;
            try
            {
                using (FileStream fs = new FileStream("ListDevices.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamReader sr = new StreamReader("ListDevices.txt", false))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] arrayList = sr.ReadLine().Split('|');
                        if (arrayList.Length >= 3)
                        {
                            if (int.TryParse(arrayList[1], out iIndex) && arrayList[0] == deviceCode)
                            {
                                fileIndex = arrayList[2];
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return fileIndex;
        }

        public static List<string> GetDevicesFromFile()
        {
            List<string> lstDevices = new List<string>();

            string fileIndex = "31";
            int iIndex = 0;

            using (FileStream fs = new FileStream("ListDevices.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamReader sr = new StreamReader("ListDevices.txt", false))
            {
                while (!sr.EndOfStream)
                {
                    string[] arrayList = sr.ReadLine().Split('|');
                    if (!string.IsNullOrEmpty(arrayList[0]))
                    {
                        lstDevices.Add(arrayList[0]);
                    }
                }
            }

            return lstDevices;
        }

        public static string GetCountryRandom()
        {
            string fileIndex = "Viet Nam|vn";
            List<string> lstCountry = new List<string>();
            try
            {
                using (FileStream fs = new FileStream("countriestop.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
                using (StreamReader sr = new StreamReader("countriestop.txt", false))
                {
                    while (!sr.EndOfStream)
                    {
                        string arrayList = sr.ReadLine();
                        if (!string.IsNullOrEmpty(arrayList))
                        {
                            lstCountry.Add(arrayList);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            if (lstCountry.Count > 0)
            {
                fileIndex = lstCountry[new Random().Next(0, lstCountry.Count - 1)];
            }

            return fileIndex;
        }
    }
}
