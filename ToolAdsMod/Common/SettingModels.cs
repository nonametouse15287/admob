﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public class SettingModels
    {
        public string txtServerKey { get; set; }
        public string txtMinute { get; set; }
        public bool chkChangeModem { get; set; }
        public string txtApiKey { get; set; }
        public string NamePath { get; set; }
        public string ThreadNumber { get; set; }
        public string NoxFolder { get; set; }
        public bool chkHideChrome { get; set; }
        public bool chkHideCommand { get; set; }
    }
}
