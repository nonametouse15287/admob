﻿using Newtonsoft.Json;
using RestSharp;
using System.Configuration;
using ToolAdsMod.Models;

namespace ToolAdsMod.Common
{
    public static class ApiHelper
    {
        static string DomainServer = ConfigurationManager.AppSettings["DomainServer"].ToString();

        public static string GmailGet(string keyapi)
        {
            var client = new RestClient($"{DomainServer}api/gmail/gmailpush?token={keyapi}&amount=1&type=1h");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response.ToString();
        }

        public static AndroidDevicesResult DevicesGet(string CountryCode)
        {
        GETDEVICES:
            AndroidDevicesResult objResult = new AndroidDevicesResult();
            var client = new RestClient($"{DomainServer}api/Devices/DevicesGet?CountryCode={CountryCode}");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            try
            {
                objResult = null;
                ResultDetail objResultData = new ResultDetail();
                objResultData = JsonConvert.DeserializeObject<ResultDetail>(response.Content.ToString());

                if (objResultData.Details != null)
                {
                    objResult = JsonConvert.DeserializeObject<AndroidDevicesResult>(objResultData.Details.ToString());
                }
            }
            catch (System.Exception)
            {
            }

            if (objResult == null || objResult.DeviceInfo == null)
            {
                goto GETDEVICES;
            }
            return objResult;
        }

        public static string GetBalanceAPI(string key)
        {
            var client = new RestClient($"https://2captcha.com/res.php?key={key}&action=getbalance");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response.Content.ToString();
        }

    }
}
