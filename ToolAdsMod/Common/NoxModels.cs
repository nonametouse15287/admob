﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public class NoxModels
    {
        public int NoxIndex { set; get; }
        public string NoxFolder { set; get; }
        public string NoxPort { set; get; }
        public string KeyRandom { set; get; }
        public List<int> NoxProcessId { set; get; }
    }
}
