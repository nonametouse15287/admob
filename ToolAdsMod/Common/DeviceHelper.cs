﻿using SharpAdbClient;
using System;
using System.Collections.Generic;

namespace ToolAdsMod.Common
{
    public static class DeviceHelper
    {
        public static List<DeviceData> GetListDevicesAdb()
        {
            List<DeviceData> lstDevices = new List<DeviceData>();
            try
            {
                AdbServer server = new AdbServer();
                var result = server.StartServer(@"adb.exe", restartServerIfNewer: false);
                lstDevices = AdbClient.Instance.GetDevices();
            }
            catch (Exception error)
            {
                FileSystemHelper.SaveFileErrorLog(string.Format("{0:ddMMyyyy hhmmss}|LoginEmail_Error", DateTime.Now) +
                 error.Message + "\n InnerException:" + (error.InnerException != null ? error.InnerException.Message : "") +
                 "\n StackTrace:" + error.StackTrace, "LoiHeThong");
            }
            return lstDevices;
        }
            
    }
}
