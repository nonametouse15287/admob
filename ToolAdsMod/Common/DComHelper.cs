﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolAdsMod.Common
{
    public static class DComHelper
    {        
     
        public static void StopAdb()
        {
            try
            {
                Process[] GetPArry = Process.GetProcessesByName("adb.exe");
                foreach (Process testProcess in GetPArry)
                {
                    try
                    {
                        testProcess.Kill();
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        
        public static string RunCMDNotWait(string cmd)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            return "";
        }

        public static string RunApp(string cmd)
        {
            try
            {
                Process cmdProcess;
                cmdProcess = new Process();
                cmdProcess.StartInfo.FileName = cmd;
                cmdProcess.StartInfo.RedirectStandardOutput = true;
                cmdProcess.StartInfo.UseShellExecute = false;
                cmdProcess.StartInfo.CreateNoWindow = true;
                cmdProcess.Start();
            }
            catch (Exception)
            {

                throw;
            }
            return "";
        }

        public static string RunCMD(string cmd)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            string output = cmdProcess.StandardOutput.ReadToEnd();
            cmdProcess.WaitForExit(30000);
            if (String.IsNullOrEmpty(output))
                return "";
            Thread.Sleep(200);
            return output;
        }
        public static string RunCMDProcessId(string cmd, out int processId)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            processId = cmdProcess.Id;
            return "";
        }

        public static void RunGoogleClean(string command)
        {
            using (Process proc = new Process())
            {
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd.exe");
                procStartInfo.RedirectStandardInput = true;
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.RedirectStandardError = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.StandardInput.WriteLine(command);
                proc.StandardInput.WriteLine("su"); // this will not work on some newer os
                proc.StandardInput.WriteLine("rm -f /data/data/com.google.android.gms/shared_prefs/adid_settings.xml"); // this will not work on some newer os                
                proc.StandardInput.WriteLine("exit");
                proc.StandardInput.WriteLine("exit");
                proc.WaitForExit(10000);
            } // this might not be correct wait command

        }

        public static void RunCreateFolder(string command)
        {
            using (Process proc = new Process())
            {
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd.exe");
                procStartInfo.RedirectStandardInput = true;
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.RedirectStandardError = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.StandardInput.WriteLine(command);
                proc.StandardInput.WriteLine("su"); // this will not work on some newer os
                proc.StandardInput.WriteLine("mkdir /sdcard/devicechanger"); // this will not work on some newer os
                proc.StandardInput.WriteLine("mkdir -m 777 /sdcard/devicechanger");
                proc.WaitForExit(10000);
            } // this might not be correct wait command


        }

        public static string RunCMDV2(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit(30000);

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");

            process.Close();
            Thread.Sleep(200);
            return output;
        }

        public static string RunCMDV2(string command, ref string output, ref string err)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit(30000);

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            output = process.StandardOutput.ReadToEnd();
            err = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(err) ? "(none)" : err));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");

            process.Close();
            Thread.Sleep(200);
            return output;
        }

        public static string RunCMDReturnResult(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit(30000);

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;
            process.Close();
            return output + error;
        }
      
        public static bool isDuplicate(string ip)
        {
            using (FileStream fs = new FileStream("ip.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            if (File.ReadAllText("ip.txt").Contains(ip))
            {
                return true;
            }
            File.AppendAllText("ip.txt", ip + Environment.NewLine);
            return false;
        }

        public static void CleanFileIP()
        {
            using (FileStream fs = new FileStream("ip.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            File.WriteAllText("ip.txt", String.Empty);
        }

        public static bool ChangeAutoIP(string cbbmang)
        {
            string disconnect = RunCMD("Rasdial /disconnect");
            if (!disconnect.Contains("Command completed"))
            {
                return false;
            }
            Thread.Sleep(1000);
            string connect = RunCMD("Rasdial " + cbbmang);
            if (connect.Contains("modem was not found"))
            {
                MessageBox.Show("Modem was not found", "MerchAMZreg ERROR");
                return false;
            }
            else if (connect.Contains("Access error 623"))
            {
                MessageBox.Show("Sai tên nhà mạng", "MerchAMZreg ERROR");
                return false;
            }
            else if (connect.Contains("Successfully connected to"))
            {
                return true;
            }
            return false;
        }

        public static string GetAutoIP()
        {
            string output = RunCMD("nslookup myip.opendns.com. resolver1.opendns.com");
            var ip = Regex.Matches(output, @"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b");
            if (ip.Count == 2)
            {
                return ip[1].Value;
            }
            else
            {
                return "";
            }
        }
             

    }
}
