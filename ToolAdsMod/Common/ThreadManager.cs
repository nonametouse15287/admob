﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolAdsMod.Common
{
    public class ThreadManager
    {
        public List<ThreadInfo> GenerListThreads { get; set; } = new List<ThreadInfo>();
    }

    public class ThreadInfo
    {
        public int Status { get; set; }
        public ListViewItem DevicesItems { get; set; }
        public string DevicesSerial { get; set; }
        public string StatusStep { get; set; }
        public string DevicesProxy { get; set; }
        public Thread GenThreadInfo { get; set; }
    }
}
