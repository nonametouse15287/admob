﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public static class NoxHelpers
    {
        public static void killAll(List<int> listIdProcess, int processAdbId)
        {
            try
            {
                foreach (var item in listIdProcess)
                {
                    if (item != processAdbId)
                    {
                        KillProcess(item);
                    }
                }

            }
            finally
            {

            }
        }

        public static void KillProcess(int pid)
        {
            try
            {
                Process[] array2 = Process.GetProcesses();
                foreach (var item in array2)
                {
                    if (item.Id == pid)
                    {
                        try
                        {
                            item.Kill();
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                //Exception exception1 = exception1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mNoxExeDir">D:\Program Files\Nox\bin\Nox.exe</param>
        /// <param name="mNoxName"></param>
        public static int renewNox(string mNoxExeDir, string mNoxName, string mAdbPath, string mDeviceIp)
        {
            DComHelper.RunCMDV2($"{mNoxExeDir} -clone:{mNoxName} -quit");
            System.Threading.Thread.Sleep(2000);
            int processid = 0;
            DComHelper.RunCMDProcessId($"{mNoxExeDir} -clone:{mNoxName} -resolution:720x1280 -root:true", out processid);
            return processid;
            //
            //DComHelper.RunCMDV2($"{mAdbPath} -s {mDeviceIp} shell getprop sys.boot_completed");
        }

    }
}
