﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolAdsMod.Common
{
    public static class CheckStepHelper
    {

        public static string currentCommand { set; get; }

        static string currentExPath = Path.Combine(Application.StartupPath, "ImageCheckCrop");

        public static bool CheckOrange(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(450, 1020);
            Size sizeCaptcha = new Size(120, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckOrange.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static bool CheckOrange2(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(450, 1020);
            Size sizeCaptcha = new Size(120, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckOrange2.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static bool CheckDeleteAccount(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(120, 720);
            Size sizeCaptcha = new Size(140, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckDeleteAccount.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static bool CheckEmailKhoiPhuc(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(230, 200);
            Size sizeCaptcha = new Size(120, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckEmailKhoiPhuc.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static bool CheckPlayStoreAccept(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(800, 1084);
            Size sizeCaptcha = new Size(145, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckPlayStoreAccept.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static bool CheckPlayStoreError(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            bool checkIm = false;
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(800, 1084);
            Size sizeCaptcha = new Size(145, 35);

            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckPlayStoreError.png");
            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static string CheckAllStepRegAccount(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(60, 200);
            Size sizeCaptcha = new Size(550, 60);
            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            string base64 = ImageSearchHelpers.ConvertImageToBase64(Path.Combine(currentExPath, imageCheckCrop));

            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);

            return base64;
        }

        public static bool CheckStepAccountSuccess(string fileImages, string host)
        {
            FileSystemHelper.CreateFolder(fileImages);
            string imageCheckCrop = string.Format("{0}.png", Guid.NewGuid());
            string imageCheckCreate = string.Format("{0}.png", Guid.NewGuid());
            string imageOrigin = Path.Combine(fileImages, imageCheckCreate);
            ChupAnhManHinh(fileImages, imageCheckCreate, host);
            Thread.Sleep(500);
            Point pointCaptcha = new Point(920, 300);
            Size sizeCaptcha = new Size(75, 80);
            ImageSearchHelpers.CropImage(imageOrigin, Path.Combine(currentExPath, imageCheckCrop), pointCaptcha, sizeCaptcha);
            bool checkIm = ImageSearchHelpers.FindImages(Path.Combine(currentExPath, imageCheckCrop), @"step_image\CheckStepAccountSuccess.png");

            DeleteImage(Path.Combine(currentExPath, imageCheckCrop));
            DeleteImage(imageOrigin);
            return checkIm;
        }

        public static void DeleteImage(string imageOrigin)
        {
            try
            {
                System.IO.File.Delete(imageOrigin);
            }
            catch (Exception ex)
            {

            }
        }

        public static void ChupAnhManHinh(string fileImages, string fileName, string host)
        {
            //**Delete file Images
            try
            {
                System.IO.File.Delete(Path.Combine(fileImages, fileName));
            }
            catch (Exception)
            {

            }
            ////Captcha
            System.IO.File.WriteAllText(fileImages + @"\Adb.bat", $"{currentCommand} -s {host} pull /sdcard/{fileName} \"" + fileImages + "\"");
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell screencap -p /sdcard/{fileName}");
            Thread.Sleep(500);
            DComHelper.RunCMDV2(fileImages + @"\Adb.bat");
            Thread.Sleep(500);
            DComHelper.RunCMDV2($"{currentCommand} -s {host} shell rm -f /sdcard/{fileName}");
            Thread.Sleep(500);
        }

        public static void CopyFileToAndroid(string fileImages, string host)
        {
            string localdb = Path.Combine(Application.StartupPath, "accounts.db");
            System.IO.File.WriteAllText(fileImages + @"\AdbCopyFileToAndroid.bat", $"{currentCommand} -s {host} push \"" + localdb + $"\" /data/system/users/0/");
            Thread.Sleep(500);
            DComHelper.RunCMDV2(fileImages + @"\AdbCopyFileToAndroid.bat");
            Thread.Sleep(500);
        }

       
    }
}
