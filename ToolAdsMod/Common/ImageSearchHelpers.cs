﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolAdsMod.Common
{
    public static class ImageSearchHelpers
    {
        static string dllPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ImageSearchDLL.dll";
        [DllImport("ImageSearchDLL.dll")]
        private static extern IntPtr ImageSearch(int x, int y, int right, int bottom, [MarshalAs(UnmanagedType.LPStr)]string imagePath);

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        public static extern bool SetDllDirectory([MarshalAs(UnmanagedType.LPStr)] String pathName);

        public static PositionXY UseImageSearch(string imgPath)
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            /*Read width and height*/
            int imageWidth = 0;
            int imageHeight = 0;
            using (Bitmap b = new Bitmap(appPath + imgPath))
            {
                imageWidth = b.Width;
                imageHeight = b.Height;
            }

            PositionXY dataReturn = new PositionXY();

            imgPath = "*" + "30 " + appPath + imgPath;

            int right = Screen.PrimaryScreen.WorkingArea.Right;
            int bottom = Screen.PrimaryScreen.WorkingArea.Bottom;

            IntPtr result = ImageSearch(0, 0, right, bottom, imgPath);
            String res = Marshal.PtrToStringAnsi(result);

            if (res[0] == '0') return null;
            String[] data = res.Split('|');
            int x; int y;
            int.TryParse(data[1], out x);
            int.TryParse(data[2], out y);
            dataReturn.X = x + (imageWidth / 2);
            dataReturn.Y = y + (imageHeight / 2);
            return dataReturn;
        }

        public static string CropImage(string fileName, string outFilePath, Point point, Size size)
        {
            try
            {
                Rectangle cropRect = new Rectangle(point, size);
                using (Bitmap src = Image.FromFile(fileName) as Bitmap)
                {
                    using (Bitmap target = new Bitmap(cropRect.Width, cropRect.Height))
                    {
                        using (Graphics g = Graphics.FromImage(target))
                        {
                            g.DrawImage(src, new Rectangle(0, 0, target.Width, target.Height), cropRect, GraphicsUnit.Pixel);
                        }
                        target.Save(outFilePath);
                    }
                }
            }
            catch (Exception)
            {

            }

            return "";
        }

        public static string ConvertImageToBase64(string Path)
        {
            using (Image image = Image.FromFile(Path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public static string ConvertImageToBase64FromImage(Image image)
        {
            using (image)
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public class PositionXY
        {
            public int X { set; get; }
            public int Y { set; get; }
        }

        public static bool FindImages(string filepathA, string filepathB)
        {
            try
            {
                decimal iTotalPoint = 0;
                decimal iTotalPointFail = 0;
                // Load the images.
                Bitmap bm1 = new Bitmap(filepathA);
                Bitmap bm2 = new Bitmap(filepathB);

                // Make a difference image.
                int wid = Math.Min(bm1.Width, bm2.Width);
                int hgt = Math.Min(bm1.Height, bm2.Height);
                Bitmap bm3 = new Bitmap(wid, hgt);

                // Create the difference image.
                bool are_identical = true;
                Color eq_color = Color.White;
                Color ne_color = Color.Red;
                for (int x = 0; x < wid; x++)
                {
                    for (int y = 0; y < hgt; y++)
                    {
                        iTotalPoint = iTotalPoint + 1;
                        if (bm1.GetPixel(x, y).Equals(bm2.GetPixel(x, y)))
                            bm3.SetPixel(x, y, eq_color);
                        else
                        {
                            bm3.SetPixel(x, y, ne_color);

                            iTotalPointFail = iTotalPointFail + 1;
                        }
                    }
                }

                if ((iTotalPointFail * 100) / iTotalPoint > 10)
                {
                    are_identical = false;
                }
                // Display the result.

                if ((bm1.Width != bm2.Width) || (bm1.Height != bm2.Height)) are_identical = false;

                bm1.Dispose();
                bm2.Dispose();

                if (are_identical)
                {
                    return true;
                    //lblResult.Text = "The images are identical";
                }
                else
                {
                    return false;
                    //lblResult.Text = "The images are different";
                }

            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static void ResizeImage(string fromFile, int width, string outFile)
        {
            using (Image image = Image.FromFile(fromFile))
            {
                int height = ((width * 100 / image.Width) * image.Height) / 100;
                var destRect = new Rectangle(0, 0, width, height);
                var destImage = new Bitmap(width, height);

                destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                }

                destImage.Save(outFile);
            }
        }

    }
}
