﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public class DevicesModels
    {
        public string DeviceCode { get; set; }
        public string DeviceStatus { get; set; }
    }
}
