﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace ToolAdsMod.Common
{
    public static class ChromeHelpers
    {
        public static ChromeDriver KhoiTaoChrome(ref ChromeDriver driver, string profile, bool hideCommand, bool hideChrome, string userAgent)
        {
            string profileFolder = AppDomain.CurrentDomain.BaseDirectory + "//Chrome//" + profile;
            var driverService = ChromeDriverService.CreateDefaultService();
            if (hideCommand)
            {
                driverService.HideCommandPromptWindow = true;
            }

            ChromeOptions chromeOptions = new ChromeOptions();

            if (hideChrome)
            {
                chromeOptions.AddArguments("--headless");
            }
            chromeOptions.AddArgument("--user-data-dir=" + profileFolder);
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--disable-infobars");
            chromeOptions.AddArgument("--disable-notifications");
            chromeOptions.AddArgument("incognito");
            chromeOptions.AddArgument($"--user-agent={userAgent}");
            driver = new ChromeDriver(driverService, chromeOptions);

            return driver;
        }

        public static void KillChromeDriver(ChromeDriver driver)
        {
            try
            {
                driver.Close();
                driver.Quit();
                driver.Dispose();
            }
            catch
            {
                // ignored
            }
        }

        public static void FindElementWait(this ChromeDriver driver, By elementBy, int timeout)
        {
            int iCountTime = 0;
        BEGINWAIT:
            try
            {
                iCountTime = iCountTime + 1;
                driver.FindElement(elementBy);
            }
            catch (Exception error)
            {
                FileSystemHelper.SaveFileErrorLog(string.Format("{0:ddMMyyyy hhmmss}|TaoEmail_Thread_Nghi", DateTime.Now) +
                       error.Message + "\n InnerException:" + (error.InnerException != null ? error.InnerException.Message : "") +
                       "\n StackTrace:" + error.StackTrace, "LoiHeThong");

                if (iCountTime < timeout)
                {
                    Thread.Sleep(1000);
                    goto BEGINWAIT;
                }
            }
        }
    }
}
