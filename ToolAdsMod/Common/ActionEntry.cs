﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolAdsMod.Common
{
    public static class ConstantFields
    {
        public const int MOUSEEVENTF_MOVE = 0x0001; /* mouse move */
        public const int MOUSEEVENTF_LEFTDOWN = 0x0002; /* left button down */
        public const int MOUSEEVENTF_LEFTUP = 0x0004; /* left button up */
        public const int MOUSEEVENTF_RIGHTDOWN = 0x0008; /* right button down */
        public const int MOUSEEVENTF_RIGHTUP = 0x0010; /* right button up */
        public const int MOUSEEVENTF_MIDDLEDOWN = 0x0020; /* middle button down */
        public const int MOUSEEVENTF_MIDDLEUP = 0x0040; /* middle button up */
        public const int MOUSEEVENTF_XDOWN = 0x0080; /* x button down */
        public const int MOUSEEVENTF_XUP = 0x0100; /* x button down */
        public const int MOUSEEVENTF_WHEEL = 0x0800; /* wheel button rolled */
        public const int MOUSEEVENTF_VIRTUALDESK = 0x4000; /* map to entire virtual desktop */
        public const int MOUSEEVENTF_ABSOLUTE = 0x8000; /* absolute move */
    }
    public enum ClickType
    {
        click = 0,
        rightClick = 1,
        doubleClick = 2,
        SendKeys = 3
    }
    public class ActionEntry
    {
        int x;
        int y;
        string text;
        int interval;
        ClickType type;
        public ActionEntry(int x, int y, string text, int interval, ClickType type)
        {
            this.x = x;
            this.y = y;
            this.text = text;
            this.interval = interval;
            this.type = type;
        }

        public int X
        {
            set { x = value; }
            get { return x; }
        }
        public int Y
        {
            set { y = value; }
            get { return y; }
        }
        public string Text
        {
            set { text = value; }
            get { return text; }
        }

        public int Interval
        {
            set { interval = value; }
            get { return interval; }
        }
        public ClickType Type
        {
            set { type = value; }
            get { return type; }
        }
    }
}
