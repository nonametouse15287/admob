﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ToolAdsMod.Common.ImageSearchHelpers;

namespace ToolAdsMod.Common
{
    public static class FindImageClick
    {

        public static bool ActiveTopNoxPlay()
        {
            bool checkOpen = false;
            return true;
        }

        public static PositionXY _0_clickmore(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\0.ClickMore.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _1_start_services(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\1.start_services.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _2_clickone_clean(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\2.ClickOne_Clean.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _3_clickok(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\3.ClickOk.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _4_click_addaccount(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\4.Click_AddAccount.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                int iCount = 0;
                while (obj == null)
                {
                    obj = WaitImage(time, imagePath);
                }
            }

            return obj;
        }
        public static PositionXY _5_click_icongoogle(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\5.Click_IconGoogle.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _6_clickcreate(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\6.ClickCreate.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _7_clickmyselft(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\7.ClickMyselft.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _8_clickfirstname(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\8.ClickFirstName.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _9_clicklastname(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\9.ClickLastName.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _10_clicknext(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\10.ClickNext.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _11_clickday(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\11.ClickDay.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _12_clickmoth(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"12.ClickMoth.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _13_clickchoosemonth(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"13.ClickChooseMonth.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                int iCount = 0;
                while (obj == null)
                {
                    obj = WaitImage(time, imagePath);
                }
            }

            return obj;
        }
        public static PositionXY _14_clickyear(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\14.ClickYear.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _15_clickgender(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\15.ClickGender.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _16_clickchoosegender(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\16.ClickChooseGender.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _17_clicknext(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\17.ClickNext.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _18_clickusername(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\18.ClickUserName.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _19_clicknext(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\19.ClickNext.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _20_clickpassword(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\20.ClickPassword.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _21_clickconfirm(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\21.ClickConfirm.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _22_clickgoolgle(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\22.ClickGoolgle.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _23_clickskip(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\23.ClickSkip.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _24_clickgoolgle(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\24.ClickGoolgle.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _25_clickiagree(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\25.ClickIagree.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }

            return obj;
        }
        public static PositionXY _26_clickthanks(int time)
        {
            ActiveTopNoxPlay();
            string imagePath = @"pic_gmail\26.ClickThanks.PNG";
            PositionXY obj = UseImageSearch(imagePath);
            if (obj == null)
            {
                obj = WaitImage(time, imagePath);
            }
            return obj;
        }

        public static PositionXY WaitImage(int time, string imagePath)
        {
            PositionXY obj = null;
            int iCount = 0;
            while (obj == null)
            {
                iCount = iCount + 1;
                if (iCount < time)
                {
                    Thread.Sleep(1000);
                    ActiveTopNoxPlay();
                    obj = UseImageSearch(imagePath);
                }
            }
            return obj;
        }

    }
}
