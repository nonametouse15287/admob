﻿namespace ToolAdsMod
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lstViewDevices = new System.Windows.Forms.ListView();
            this.clDeviceSerial = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clStepStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clProxy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnStart = new System.Windows.Forms.ToolStripMenuItem();
            this.mnStop = new System.Windows.Forms.ToolStripMenuItem();
            this.mnRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtServerKey = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.Red;
            this.btnStop.Location = new System.Drawing.Point(289, 237);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 42);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "STOP ALL";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Blue;
            this.btnSave.Location = new System.Drawing.Point(395, 237);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 42);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lstViewDevices
            // 
            this.lstViewDevices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clDeviceSerial,
            this.clStatus,
            this.clStepStatus,
            this.clName,
            this.clProxy});
            this.lstViewDevices.ContextMenuStrip = this.contextMenuStrip1;
            this.lstViewDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstViewDevices.FullRowSelect = true;
            this.lstViewDevices.GridLines = true;
            this.lstViewDevices.Location = new System.Drawing.Point(3, 16);
            this.lstViewDevices.MultiSelect = false;
            this.lstViewDevices.Name = "lstViewDevices";
            this.lstViewDevices.Size = new System.Drawing.Size(523, 174);
            this.lstViewDevices.TabIndex = 0;
            this.lstViewDevices.UseCompatibleStateImageBehavior = false;
            this.lstViewDevices.View = System.Windows.Forms.View.Details;
            // 
            // clDeviceSerial
            // 
            this.clDeviceSerial.Text = "DEVICES SERIAL";
            this.clDeviceSerial.Width = 108;
            // 
            // clStatus
            // 
            this.clStatus.Text = "STATUS";
            this.clStatus.Width = 73;
            // 
            // clStepStatus
            // 
            this.clStepStatus.Text = "STEP STATUS";
            this.clStepStatus.Width = 177;
            // 
            // clName
            // 
            this.clName.Text = "Name";
            this.clName.Width = 94;
            // 
            // clProxy
            // 
            this.clProxy.Text = "PROXY";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnStart,
            this.mnStop,
            this.mnRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(116, 70);
            // 
            // mnStart
            // 
            this.mnStart.Name = "mnStart";
            this.mnStart.Size = new System.Drawing.Size(115, 22);
            this.mnStart.Text = "START";
            this.mnStart.Click += new System.EventHandler(this.mnStart_Click);
            // 
            // mnStop
            // 
            this.mnStop.Name = "mnStop";
            this.mnStop.Size = new System.Drawing.Size(115, 22);
            this.mnStop.Text = "STOP";
            this.mnStop.Click += new System.EventHandler(this.mnStop_Click);
            // 
            // mnRemove
            // 
            this.mnRemove.Name = "mnRemove";
            this.mnRemove.Size = new System.Drawing.Size(115, 22);
            this.mnRemove.Text = "REMOVE";
            this.mnRemove.Click += new System.EventHandler(this.mnRemove_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstViewDevices);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 193);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Devices";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(547, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(173, 267);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Server key";
            // 
            // txtServerKey
            // 
            this.txtServerKey.Location = new System.Drawing.Point(103, 12);
            this.txtServerKey.Name = "txtServerKey";
            this.txtServerKey.Size = new System.Drawing.Size(388, 20);
            this.txtServerKey.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 284);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtServerKey);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnStop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "ToolAdsMod - V1.9";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListView lstViewDevices;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ColumnHeader clDeviceSerial;
        private System.Windows.Forms.ColumnHeader clStatus;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ColumnHeader clStepStatus;
        private System.Windows.Forms.ToolStripMenuItem mnStart;
        private System.Windows.Forms.ToolStripMenuItem mnStop;
        private System.Windows.Forms.ColumnHeader clName;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem mnRemove;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtServerKey;
        private System.Windows.Forms.ColumnHeader clProxy;
    }
}

